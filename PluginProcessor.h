#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "DspFilters/Dsp.h"
#define F_ORDER 12 

//==============================================================================
/**
*/
class FreeClipAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    FreeClipAudioProcessor();
    ~FreeClipAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

	float calcCutoff();

	AudioParameterInt* p_wavetype;
	AudioParameterFloat* p_ceilingf;
	AudioParameterInt* p_oversample;
	AudioParameterFloat* p_gainf;
	float m_softness = 0.0;
	float m_postceilingf = 1.0;
	AudioParameterFloat* p_outGainf;

	double m_fsamplerate;
	Dsp::SimpleFilter <Dsp::Butterworth::LowPass <F_ORDER>, 2> f;
	Dsp::SimpleFilter <Dsp::Butterworth::LowPass <F_ORDER>, 2 > f2;

	const int maxOversample = 32;
	const float sampleShift = 0.0;

	bool m_settingsChanged = true;
	bool m_postOversamplingClip;
	int m_postClipType = 1;

	bool m_settingsLoaded = false;

	LevelMeterSource levelMeterSourceIn;
	LevelMeterSource levelMeterSourceOut;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (FreeClipAudioProcessor)

	AudioSampleBuffer* m_bufferResized;
	int m_ioversample = 1;
	float m_prevceil = 0;
	float m_prevgain = 1.0;
	float m_prevoutGain = 1.0;
	int m_prevovr = 1;

	int m_forder = F_ORDER;

	void clipSamples(AudioSampleBuffer* buffer, int numchans);
	void overSampleZS(AudioSampleBuffer* oldBuffer, AudioSampleBuffer* newBuffer, int numchans);
	void decimate(AudioSampleBuffer* upBuffer, AudioSampleBuffer* downBuffer, int numchans);

	float sinclip(float &s);
	float logiclip(float &s);
	float hardclip(float &s);
	float tanclip(float &s, float &soft);
	float quintic(float &s);
	float cubicBasic(float &s);
	float algclip(float &s, float soft);
	float arcClip(float &s, float &soft);
};


#endif  // PLUGINPROCESSOR_H_INCLUDED
